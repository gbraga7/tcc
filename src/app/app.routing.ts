import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { LoginComponent } from './core/login/login.component';
import { RegisterComponent } from './core/register/register.component';
import { StarterComponent } from './starter/starter.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: StarterComponent
  },
  {
    path: 'app',
    component: FullComponent,
    children: [
      {
        path: '',
        redirectTo: '/starter',
        pathMatch: 'full'
      },
      {
        path: '',
        loadChildren: './material-component/material.module#MaterialComponentsModule'
      },
      {
        path: '',
        loadChildren: './pages/pages.module#PagesModule'
      },
      {
        path: 'starter',
        loadChildren: './starter/starter.module#StarterModule'
      },
      // { path: '', redirectTo: 'login', pathMatch: 'full' },
      // { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
      // { path: 'register', component: RegisterComponent, canActivate: [AuthGuard] },
      // { path: 'user', component: UserComponent,  resolve: { data: UserResolver}}
    ]
  }
];

