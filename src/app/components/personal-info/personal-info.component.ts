import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { State } from '../../models/state/state';
import { User } from '../../models/user/user.model';
import { FileService } from '../../services/file.service';
import { environment } from '../../../environments/environment';
import { UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';


@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss'],
})
export class PersonalInfoComponent implements OnInit {

  states: State[];
  formPersonalInfo: FormGroup;
  loading = false;
  submitted = false;
  @Input() user: User;

  constructor(private file: FileService,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private alertService: AlertService,
              private fileService: FileService) { }

  ngOnInit() {
    this.formPersonalInfo = this.formBuilder.group({
      genre: [''],
      name: [''],
      birthdate: [''],
      country: [''],
      state: [''],
      city: [''],
      address: [''],
      cep: [''],
      email: [''],
      phoneNumber: [''],
      occupation: [''],
      isWriter: [''],
      isSaler: [''],
      username: [''] // nem uso
    });

    this.file.getJSON(environment.filesUrl + '/states.json').subscribe(data => {
      this.states = data;
    });

    this.formPersonalInfo.patchValue(this.user);

    if (this.user.birthdate !== null) {
      this.formPersonalInfo.patchValue({
        birthdate: this.user.birthdate.toDate(),
      });
    }

    console.log('Personal Info Component');
  }

  onSubmit() {
    this.submitted = true;

    if (!this.formPersonalInfo.valid) {
      this.alertService.showMessage('Form is invalid. Please, correct the fields and try again.');
    }

    this.loading = true;

    this.userService.update(this.formPersonalInfo.value)
    .then(data => {
      this.loading = false;
      this.alertService.showMessage('Save with success!');
    }).catch(error => {
      this.loading = false;
      this.alertService.showMessage(error);
    });
  }

  fileClick(input) {
    input.click();
  }

  upload(files) {
    // console.log(input.value);
    // this.user.photoURL = input.value;
    // const url = environment.uploadsUrl + '/images';
    this.fileService.uploadAvatar(files);
    // this.fileService.upload('http://localhost:4200/assets', files);
    // this.fileService.upload('https://file.io', files);
  }

}
