import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { first } from 'rxjs/operators';
import { MyErrorStateMatcher } from '../form-helper/my-error-state-matcher';

import { RegisterComponent } from '../register/register.component';

import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  matcher = new MyErrorStateMatcher();
  viewMessage: string;

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private dialog: MatDialog,
      private dialogRef: MatDialogRef<LoginComponent>,
      private snackBar: MatSnackBar,
      private auth: AuthService,
      private userService: UserService,
      private alertService: AlertService,
      private zone: NgZone) {}

  ngOnInit() {
      this.loginForm = this.formBuilder.group({
          email: ['', Validators.required],
          password: ['', Validators.required]
      });

      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
        return;
    }

    this.loading = true;

    this.auth.doLogin(this.f.email.value, this.f.password.value)
      .then(data => {
          this.zone.run(() => {
            this.dialogRef.close();
            this.router.navigateByUrl('/app/dashboard');
          });
      }, error => {
        this.zone.run(() => {
          this.loading = false;
          this.viewMessage = error.message;
          this.alertService.showMessage(this.viewMessage, 'Login with Google');
        });
      });
  }

  openRegisterForm() {
    this.dialogRef.close();
    const dialogRegister = this.dialog.open(RegisterComponent, {
      width: '700px',
      data: {  }
    });
  }

  registerByGoogle() {
    this.userService.registerByGoogle()
    .then(res => {
      if (res === true) {
        this.viewMessage = 'Welcome to the Plato Pro!';
      } else {
        this.viewMessage = res;
      }

      this.zone.run(() => {
        this.dialogRef.close();
        this.router.navigateByUrl('/app/dashboard');
      });

      this.alertService.showMessage(this.viewMessage, 'Login with Google');

    }, error => {
      this.viewMessage = error.message;
      this.alertService.showMessage(this.viewMessage, 'Login with Google');
    });
  }

  registerByFacebook() {
    this.userService.registerByFacebook()
    .then(res => {
      if (res === true) {
        this.viewMessage = 'Welcome to the Plato Pro!';
      } else {
        this.viewMessage = res;
      }

      this.alertService.showMessage(this.viewMessage, 'Login with Facebook');

      this.zone.run(() => {
        this.dialogRef.close();
        this.router.navigateByUrl('/app/dashboard');
      });

    }, error => {
      console.log(error);
      this.viewMessage = error.message;
      this.alertService.showMessage(this.viewMessage, 'Login with Facebook');
    });
  }
}
