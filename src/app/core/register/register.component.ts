import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { MatDialogRef } from '@angular/material';
import { SnackbarComponent } from '../../material-component/snackbar/snackbar.component';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    registerForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    viewMessage: string;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private dialogRef: MatDialogRef<RegisterComponent>,
              private snackBar: MatSnackBar,
              private zone: NgZone) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
        name: ['', Validators.required],
        birthdate: ['', Validators.required],
        email: ['', Validators.required],
        phoneNumber: ['', Validators.required],
        username: ['', Validators.required],
        password: ['', Validators.required]
    });

    this.returnUrl = '#/app/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.userService.register(this.registerForm.value)
    .then(res => {
      console.log('onSubmit');
      console.log(res);

      if (res === true) {
        this.viewMessage = 'Your account has been created';
      } else {
        this.viewMessage = res;
      }

      this.snackBar.open(this.viewMessage, 'Register', {
        duration: 5000,
      });

      this.zone.run(() => {
        this.dialogRef.close();
        this.router.navigateByUrl('/app/dashboard');
      });

    }, error => {
      console.log(error);
      this.viewMessage = error.message;
    });
  }

  registerByGoogle() {
    this.userService.registerByGoogle()
    .then(res => {
      console.log('registerByGoogle');
      console.log(res);

      if (res === true) {
        this.viewMessage = 'Your account has been created';
      } else {
        this.viewMessage = res;
      }

      this.zone.run(() => {
        this.dialogRef.close();
        this.router.navigateByUrl('/app/dashboard');
      });

      this.snackBar.open(this.viewMessage, 'Register with Google', {
        duration: 5000,
      });

    }, error => {
      console.log(error);
      this.viewMessage = error.message;
    });
  }

  registerByFacebook() {
    this.userService.registerByFacebook()
    .then(res => {
      console.log('registerByFacebook');
      console.log(res);

      if (res === true) {
        this.viewMessage = 'Your account has been created';
      } else {
        this.viewMessage = res;
      }

      this.snackBar.open(this.viewMessage, 'Register with Facebook', {
        duration: 5000,
      });

      this.zone.run(() => {
        this.dialogRef.close();
        this.router.navigateByUrl('/app/dashboard');
      });

    }, error => {
      console.log(error);
      this.viewMessage = error.message;
    });
  }

}

