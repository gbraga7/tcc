import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: []
})
export class AppHeaderComponent {

    constructor(private auth: AuthService,
                private router: Router) {}

    signout() {
      const res = this.auth.doLogout()
      if (res) {
        // usa o serviço de alerta para mostrar mensagem
        this.router.navigateByUrl('/');
      } else {
        // exibe erro
      }
    }
}
