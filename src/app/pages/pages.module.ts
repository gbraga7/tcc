import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { PagesRoutes } from './pages.routing';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { ProjectComponent } from './project/project.component';
import { CharacterComponent } from './character/character.component';
import { SceneComponent } from './scene/scene.component';
import { LocationComponent } from './location/location.component';
import { WorldComponent } from './world/world.component';
import { ItemComponent } from './item/item.component';
import { DialogComponent } from './dialog/dialog.component';
import { ChapterComponent } from './chapter/chapter.component';
import { DemoMaterialModule } from '../demo-material-module';
import { PersonalInfoComponent } from '../components/personal-info/personal-info.component';
import { AccountInfoComponent } from '../components/account-info/account-info.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PagesRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    DemoMaterialModule,

  ],
  providers: [

  ],
  entryComponents: [

  ],
  declarations: [
    ProjectComponent,
    CharacterComponent,
    SceneComponent,
    LocationComponent,
    WorldComponent,
    ItemComponent,
    DialogComponent,
    ChapterComponent,
    DashboardComponent,
    ProfileComponent,
    PersonalInfoComponent,
    AccountInfoComponent

  ]
})

export class PagesModule {}
