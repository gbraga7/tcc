import { Routes } from '@angular/router';

import { AuthGuardService } from '../services/auth-guard.service';

import { ProjectComponent } from './project/project.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { CharacterComponent } from './character/character.component';
import { SceneComponent } from './scene/scene.component';
import { LocationComponent } from './location/location.component';
import { WorldComponent } from './world/world.component';
import { ItemComponent } from './item/item.component';
import { DialogComponent as myDialogComponent } from './dialog/dialog.component';
import { ChapterComponent } from './chapter/chapter.component';

export const PagesRoutes: Routes = [
    {
      path: 'dashboard',
      component: DashboardComponent,
      canActivate: [AuthGuardService],
    },
    {
      path: 'profile',
      component: ProfileComponent,
      canActivate: [AuthGuardService],
    },
    {
      path: 'project',
      component: ProjectComponent,
      canActivate: [AuthGuardService],
    },
    {
      path: 'character',
      component: CharacterComponent,
      canActivate: [AuthGuardService],
    },
    {
      path: 'scene',
      component: SceneComponent,
      canActivate: [AuthGuardService],
    },
    {
      path: 'location',
      component: LocationComponent,
      canActivate: [AuthGuardService],
    },
    {
      path: 'world',
      component: WorldComponent,
      canActivate: [AuthGuardService],
    },
    {
      path: 'item',
      component: ItemComponent,
      canActivate: [AuthGuardService],
    },
    {
      path: 'dialog',
      component: myDialogComponent,
      canActivate: [AuthGuardService],
    },
    {
      path: 'chapter',
      component: ChapterComponent,
      canActivate: [AuthGuardService],
    },
];
