import { Component, OnInit } from '@angular/core';

import { User } from '../../models/user/user.model';
import { UserService } from '../../services/user.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  // userSubscription: Subscription;
  user: User;

  constructor(private userService: UserService) {
    this.user = this.userService.getUser();
  }

  ngOnInit() {
    this.userService.user$.subscribe(data => {
      console.log('userSubscription', data);
      this.user = data;
    });
  }


}
