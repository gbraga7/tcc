import { Component, OnInit } from '@angular/core';
import { MatChipInputEvent, getMatFormFieldPlaceholderConflictError } from '@angular/material';
import { ENTER, COMMA } from '@angular/cdk/keycodes';

import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css'],
})
export class ProjectComponent implements OnInit {

  projects: Observable<any[]>;

  project: any = {
      name: 'primeiro livro',
      title: 'História sem fim',
      genres: [
        { name: 'science fiction' },
        { name: 'romance' }
      ],
      // tslint:disable-next-line:max-line-length
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ultrices pharetra felis in pretium. Suspendisse potenti. Suspendisse ut consectetur tellus.',
      created_at: '2018-05-25',
      updated_at: '2018-06-26',
  };



  // controllers of chips
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  // Enter, comma
  separatorKeysCodes = [ENTER, COMMA];

  constructor(private db: AngularFirestore,
              private auth: AuthService) {

    // this.cd.detach();
    // this.projects = db.list('projects').valueChanges();

  }

  ngOnInit() {
    // console.log(this.auth.isAuthenticated());
  }

  addGenre(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.project.genres.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeGenre(genre: any): void {
    const index = this.project.genres.indexOf(genre);

    if (index >= 0) {
      this.project.genres.splice(index, 1);
    }
  }

  getProjects(): Observable<any[]> {
    return this.projects;
  }

  setCurrentProject(project): void {
    console.log(this.projects);

    /*if (project.current === false) {
      project.current = true;
    }*/
    this.projects.forEach(obj => {
      obj.forEach(proj => {

          if (proj.name !== project.name) {
            proj.current = false;
          } else {
            proj.current = true;
          }

        });
        // console.log(proj);

    });

    this.project = project;
  }
}
