import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private snackBar: MatSnackBar) { }

  showMessage(_message: string, _action?: string, _duration?: number) {
    if (_action !== null) {
      _action = '';
    }

    if (_duration !== null) {
      _duration = 5000;
    }

    this.snackBar.open(_message, _action, {
        duration: _duration,
      });
  }


}
