import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public afAuth: AngularFireAuth) { }

  doRegister(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
      .then(res => {
        resolve(res);
      }, error => {
        reject(error);
      });
    });
  }

  doFacebookLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.FacebookAuthProvider();

      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, error => {
        reject(error);
      });
    });
  }

  doGoogleLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');

      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      });
    });
  }

  doLogin(email: string, password: string) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
        .then(res => {
          resolve(firebase.auth().signInWithEmailAndPassword(email, password));
        }, error => {
          reject(error);
        });
    });
  }

  isAuthenticated(): Promise<boolean> {
    return this.getUserStatus()
      .then(function (uid) {
        return true;
      }, error => {
        console.log('não está logado');
        return false;
      });
  }

  getUserStatus(): Promise<string> {
    return new Promise(function (resolve, reject) {
      firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
          resolve(user.uid);
        } else {
          reject(Error('It broke'));
        }
      });
    });
  }

  doLogout(): Promise<boolean> {
    return new Promise(function (resolve, reject) {
      firebase.auth().signOut()
        .then(res => {
          resolve(true);
        }, error => {
          reject(error);
        });
      });
  }
}
