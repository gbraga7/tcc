import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';

// import * as firebase from 'firebase/app';

const config = {
  projectId: environment.firebase.projectId,
  keyFilename: environment.firebase.apiKey
};


@Injectable({
  providedIn: 'root'
})
export class FileService {
  private storageRef: any;
  private avatarsRef: any;
  percentDone: number;
  uploadSuccess: boolean;





  constructor(private http: HttpClient) {
     // Create a storage reference from our storage service
    // this.storageRef = firebase.storage().ref();
    // Create a child reference
    // this.avatarsRef = this.storageRef.child('uploads/avatars');


  }

  public getJSON(path: string): Observable<any> {
    return this.http.get(path);
  }

  upload(url: string, files: File[]) {
    // pick from one of the 4 styles of file uploads below
    this.uploadAndProgress(url, files);
  }

  uploadAndProgress(url: string, files: File[]) {
    console.log(files);
    const formData = new FormData();
    Array.from(files).forEach(f => formData.append('file', f));

    this.http.post<any>(url, formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.uploadSuccess = true;
        }
        console.log(event);
    });
  }

  uploadAvatar(newAvatar: File) {

    console.log(newAvatar);

    // Create a root reference
    // const storageRef = firebase.storage().ref();
    // Create a reference to 'mountains.jpg'
    /*const avatarRef = storageRef.child('uploads/images/avatars/' + file);

    const file = ''; // use the Blob or File API
    avatarRef.put(file).then(function(snapshot) {
      console.log('Uploaded a blob or file!');
    });
    */
  }
}

