import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Subject } from 'rxjs';

import { environment } from '../../environments/environment';
import { User } from '../models/user/user.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

    private user: User;
    private _user = new Subject<User>();
    public user$ = this._user.asObservable();

    constructor(private http: HttpClient,
                private authService: AuthService,
                public db: AngularFirestore,
                private auth: AuthService,
                private zone: NgZone) {

      console.log('Hello, UserService!');

      this.setUser();
    }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/users/` + id);
    }

    register(user: User) {
      return this.authService.doRegister(user)
        .then(res => {
          const newUser = new User();
          newUser.id = res.user.uid;
          newUser.email = res.user.email;
          newUser.name = user.name;
          newUser.username = user.username;
          newUser.phoneNumber = user.phoneNumber;
          newUser.birthdate = user.birthdate;

          // console.log('response Google', res);
          if (res.additionalUserInfo.isNewUser === true) {
            return this.add(newUser);
          }

          return true;

        }, error => {
          console.log('register error');
          console.log(error);
          return error.message;
        });
    }

    registerByGoogle() {
        return this.authService.doGoogleLogin()
        .then(res => {
          console.log('registerByGoogle success');

          const newUser = new User();
          newUser.id = res.user.uid;
          newUser.email = res.user.email;
          newUser.name = res.user.displayName;
          newUser.phoneNumber = res.user.phoneNumber;
          newUser.photoURL = res.user.photoURL;

          // console.log('response Google', res);
          if (res.additionalUserInfo.isNewUser === true) {
            return this.add(newUser);
          }

          return true;

        }, error => {
          console.log('registerByGoogle error');
          console.log(error);
          return error.message;
        });
    }

    registerByFacebook() {
        return this.authService.doFacebookLogin()
        .then(res => {
          console.log('registerByFacebook success');

          const newUser = new User();
          newUser.id = res.user.uid;
          newUser.email = res.user.email;
          newUser.name = res.user.displayName;
          newUser.phoneNumber = res.user.phoneNumber;
          newUser.photoURL = res.user.photoURL;

          // console.log('response facebook', res);
          if (res.additionalUserInfo.isNewUser === true) {
            return this.add(newUser);
          }

          return true;

        }, error => {
          console.log('registerByFacebook error');
          console.log(error);
          return error.message;
        });
    }

    update(user: User) {
      console.log(user);

      return this.auth.getUserStatus().then((uid) => {
        const userRef = this.db.collection('users').doc(uid);
        return userRef.update(user)
        .then(function() {
            console.log('Document successfully updated!');
        })
        .catch(function(error) {
            // The document probably doesn't exist.
            console.error('Error updating document: ', error);
        });

      }).catch(error => {
        console.log(error);
      });
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/users/` + id);
    }

    add(user: User) {
      const userRef = this.db.collection('users').doc(user.id); // i'm call uid of just id

      userRef.set({
        id: user.id,
        name: user.name,
        username: user.username,
        phoneNumber: user.phoneNumber,
        email: user.email,
        birthdate: user.birthdate,
        photoURL: user.photoURL
      }).then(function() {
          console.log('Document successfully written!');
          return true;
      })
      .catch(function(error) {
          console.error('Error writing document: ', error);
          return 'Error writing document: ' + error;
      });
    }

    setUser() {
      this.auth.getUserStatus().then((uid) => {
        this.db.collection('users').doc(uid).ref.get()
        .then((doc) => {
          if (doc.exists) {
            this.user = doc.data();
            this._user.next(this.user);
          } else {
            console.log('No such document!');
            return null;
          }
        }).catch(function(error) {
          console.log('Error getting document:', error);
          return null;
        });
      });
    }

    getUser() {
      return this.user;
    }

    doSavePersonalInfo(arg0: any): any {
      throw new Error('Method not implemented.');
    }

}
