import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  localeId: string;
}

const MENUITEMS = [
  { state: 'dashboard', name: 'Dashboard', type: 'link', icon: 'dashboard', localeId: '@@dashboard' },
  { state: 'project', name: 'Project', type: 'link', icon: 'folder', localeId: '@@project' },
  { state: 'chapter', name: 'Chapter', type: 'link', icon: 'book', localeId: '@@chapter' },
  { state: 'character', name: 'Character', type: 'link', icon: 'people', localeId: '@@character' },
  { state: 'scene', name: 'Scene', type: 'link', icon: 'movie_filter', localeId: '@@scene' },
  { state: 'location', name: 'Location', type: 'link', icon: 'location_city', localeId: '@@location' },
  { state: 'world', name: 'World', type: 'link', icon: 'public', localeId: '@@world' },
  { state: 'item', name: 'Item', type: 'link', icon: 'vpn_key', localeId: '@@item' },
  { state: 'dialog', name: 'Dialog', type: 'link', icon: 'question_answer', localeId: '@@dialog' },
];

@Injectable()

export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }

}
