import { Component, AfterViewInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FormControl, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { LoginComponent } from '../core/login/login.component';
import { RegisterComponent } from '../core/register/register.component';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'starter',
  templateUrl: './starter.component.html',
  styleUrls: ['./starter.component.scss']
})
export class StarterComponent implements AfterViewInit {

  constructor(public dialog: MatDialog) {}

  ngAfterViewInit() { }

  openLoginForm() {
    console.log('openLoginForm');

    const dialogLogin = this.dialog.open(LoginComponent, {
      width: '350px',
      data: {  }
    });

    dialogLogin.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  openRegisterForm() {
    console.log('openRegisterForm');

    const dialogRegister = this.dialog.open(RegisterComponent, {
      width: '700px',
      data: {  }
    });

    dialogRegister.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }
}
